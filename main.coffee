duration = 
  min : 10
  max : 30
random_duration = () -> Math.floor(duration.min + Math.random() * (duration.max - duration.min)) * 1000

iframe = document.getElementById "main"

socket = io()

pages = []
page = null

timeout = null

clear_timeout = () ->
  if timeout?
    clearTimeout timeout

load = (p) ->
  page = p
  console.log "[im@ter] load '#{p}'"
  iframe.src = "pages/#{p}/index.html"

init = (ps) ->
  pages = ps
  load_random()

select = (p) ->
  clear_timeout()
  load p

random = () ->
  ps = pages.filter((p) -> p isnt page)
  ps[Math.floor(Math.random() * ps.length)]


load_random = () ->
  clear_timeout()
  load random()
  timeout = setTimeout load_random, random_duration()

message = (m) ->
  load_random()

window.addEventListener "message", message

socket.on "init", init
socket.on "select", select