font = null

preload = () ->
  font = loadFont('./CuteFont-Regular.ttf')

onresize = () ->
  resizeCanvas innerWidth, innerHeight

setup = () ->
  createCanvas innerWidth, innerHeight, WEBGL
  onresize()

zero_pad = (t) ->
  t = t + ""
  if t.length < 2 then "0" + t else t

to_seconds = (t) ->
  t.s + t.m * 60 + t.h * 60 * 60

to_time = (s) ->
  h = Math.floor(s / 3600)
  s -= h * 3600
  m = Math.floor(s / 60)
  s -= m * 60

  h : h
  m : m
  s : s

time_difference = (a, b) ->
  to_seconds(b) - to_seconds(a)

now = () ->
  d = new Date()

  h : d.getHours()
  m : d.getMinutes()
  s : d.getSeconds()

time_string = (t) ->
  "#{zero_pad(t.h)}:#{zero_pad(t.m)}:#{zero_pad(t.s)}"

draw = () ->
  n = now()
  closing = 
    h : 22
    m : 0
    s : 0

  diff = time_difference n, closing

  et = time_string to_time diff
  nt = time_string n

  d = new Date()
  dt = d.toDateString().split(" ").slice(0, 3).join(" ")

  background 0
  rectMode CENTER
  textFont font
  textAlign CENTER, CENTER
  fs = innerWidth / 5
  fs2 = innerWidth / 32
  textSize fs
  sine = Math.sin(frameCount * 0.01)
  
  translate 0, 0, 10

  t = frameCount * 0.01

  a1 = Math.sin(t)
  a2 = Math.sin(t + Math.PI * 2.0 * 0.33333)
  a3 = Math.sin(t + Math.PI * 2.0 * 0.66666)

  # fill 100 + a1 * 100
  # textSize (fs + a1 * fs2) * 0.8
  # text dt, 0, -fs

  # fill 100 + a2 * 100
  # textSize fs + a2 * fs2
  # text nt, 0, 0

  # rectMode CORNER
  # fill 60
  # rect(- width * 0.5, - height * 0.5, width * progress, height)

  rectMode CENTER
  noStroke()
  fill 150 + a3 * 50, 30 * a3, 30 * a3
  textSize fs
  text et, 0, 0
  # textSize fs + a3 * fs2

  if diff < 1000
    open_duration = 3600 * 15
    progress = 1 - (diff / open_duration)

    translate(- width * 0.5, - height * 0.5)
    columns = 100
    p = 2
    w = (width - p - p) / columns
    for i in [0..diff]
      x = i % columns
      y = Math.floor(i / columns)
      rect x * w + p, y * w + p, (w - p - p), (w - p - p)

