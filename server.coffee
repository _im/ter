port = 8080
fs = require 'fs'
ip = require 'ip'
schedule = require 'node-schedule'

express = require "express"
promisify = require('util').promisify
exec = promisify require('child_process').exec
app = express()
http = require('http').createServer(app)
io = require("socket.io")(http, {cors : {origin : "http://localhost:" + port}})
# exec = require('child_process').exec

app.use express.static __dirname + "/"

http.listen port, () -> console.log "> http://#{ip.address()}:#{port}"


pages = fs.readdirSync "pages"

io.on "connection", (socket) ->
  console.log "client connected"
  select_arg = process.argv[2]
  if select_arg?
    console.log "selecting #{select_arg}"
    socket.emit "select", select_arg
  else
    socket.emit "init", pages


toggle_screen = (enable) ->
  exec "xset dpms force " + (if enable then "on" else "off")

sleep = () ->
  toggle_screen(false)

wakeup = () ->
  toggle_screen(true)
  io.emit "init", pages

screen_on = schedule.scheduleJob '0 0 7 * * *', wakeup

screen_off = schedule.scheduleJob '0 0 22 * * *', sleep

end_clock = schedule.scheduleJob('0 42 21 * * *', () -> io.emit("select", "clock"))

console.log pages
console.log ""
